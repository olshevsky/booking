<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class BaseController extends AbstractActionController
{
    public function onDispatch(MvcEvent $e)
    {
        if(!($this->identity()))
        {
            $this->redirect()->toRoute('user_login');
        }

        return parent::onDispatch($e);
    }
}