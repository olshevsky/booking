<?php
namespace User\Controller;

use User\Filter\FilterLogin;
use User\Filter\FilterRegistration;
use Zend\Mvc\Controller\AbstractActionController;
use Back\Entity\User;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
//use Zend\View\Helper\ViewModel;
use Zend\View\View;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    public function loginAction()
    {

        if($this->identity())
        {
            if('manager' == $this->identity()->getRole())
                return $this->redirect()->toRoute('manager');
            elseif('user' == $this->identity()->getRole())
            {
                return $this->redirect()->toRoute('user_profile');
            }
        }

        $message = 'Login or <a href="/registration">Sing up</a>';
        $request = $this->getRequest();
        $filter = new FilterLogin();
        $filter->setData($request->getPost());

        /** @var AuthenticationService $authService */
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $adapter = $authService->getAdapter();

        if($request->isPost())
        {
            if($filter->isValid())
            {
                $login = htmlspecialchars($request->getPost('login'));
                $password = htmlspecialchars($request->getPost('passwd'));
                $adapter->setIdentityValue($login);
                $adapter->setCredentialValue($password);

                /**@var $authResult \Zend\Authentication\Result */
                $authResult = $authService->authenticate();

                if($authResult->isValid())
                {
                   $message = 'Logined...';
                    if('manager' == $this->identity()->getRole())
                        return $this->redirect()->toRoute('manager');
                    elseif('user' == $this->identity()->getRole())
                    {
                        return $this->redirect()->toRoute('user_profile');
                    }
                }
                else
                {
                    $message = 'Incorrect login or password!';
                }
            }
            else
            {
                $message = 'Error: you\'ve empty fields!';
            }
        }
        return new ViewModel(array('message' => $message, 'filter' => $filter));
    }

    public function regAction()
    {
        $filter = new FilterRegistration();
        /* @var $em EntityManager*/
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $user = new User();
        $request = $this->getRequest();
        $filter->setData($request->getPost());
        if($request->isPost())
        {
            if($filter->isValid())
            {
                $user->setLogin(htmlspecialchars($request->getPost('login')));
                $user->setName(htmlspecialchars($request->getPost('name')));
                $user->setPasswd(md5($request->getPost('passwd')));
                $user->setStatus('waiting');
                $user->setRole('user');
                $user->setPrt(1);
                $em->persist($user);
                $em->flush();
                return $this->redirect()->toRoute('user_login');
            }
            else
            {
                return new ViewModel([
                    'filter' => $filter
                ]);
            }
        }
        return new ViewModel([
            'filter' => $filter
        ]);
    }

    public function logoutAction()
    {
        /** @var AuthenticationService $authService */
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $authService->ClearIdentity();
        return $this->redirect()->toRoute('user_login');
    }
    public function errorAccessAction()
    {
        return new ViewModel();
    }
}