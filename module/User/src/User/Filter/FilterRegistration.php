<?php
namespace User\Filter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\Input;
use Zend\Validator;
use Zend\Filter\HtmlEntities;
use Zend\InputFilter\InputFilter;
use Zend\Validator\InArray;
use Zend\Validator\StringLength;


class FilterRegistration extends InputFilter
{
    public function __construct()
    {
        $userLogin= new Input('login');
        $userLogin->setRequired(true);
        $userLogin
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new  StringLength(
                [
                    'min' => '3',
                    'message' => 'should not empty',
                ]
            ));

        $password= new Input('passwd');
        $password->setRequired(true);
        $password
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StringLength(
                [
                    'min' => 5,
                    'message' => 'should not empty',
                ]
            ));

        $name = new Input('name');
        $name->setRequired(true);
        $name
            ->getFilterChain()
            ->attach(new StringTrim())
            ->attach(new StringLength(
                [
                    'min' => 5,
                    'message' => 'should not empty',
                ]
            ));

        $this
            ->add($userLogin)
            ->add($password)
            ->add($name);
    }
}