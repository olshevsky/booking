<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'user_login' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/login[/]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action'     => 'login',
                    ),
                ),
            ),
            'user_logout' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/logout[/]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action'     => 'logout',
                    ),
                ),
            ),
            'user_show' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/user/show[/]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action'     => 'show',
                    ),
                ),
            ),
            'user_reg' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/registration[/]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action'     => 'reg',
                    ),
                ),
            ),
            'not_active' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/error403[/]',
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'errorAccess'
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'factories' => array(
            'Zend\Authentication\AuthenticationService' => function ($serviceManager) {
                    return $serviceManager->get('doctrine.authenticationservice.orm_default');
            },
        ),

    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
