<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Front\Controller\Home' => 'Front\Controller\HomeController',
            'Front\Controller\Api' => 'Front\Controller\ApiController',
            'Front\Controller\OrderApi' => 'Front\Controller\OrderApiController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
                'user_profile' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/user[/]',
                        'defaults' => array(
                            'controller' => 'Front\Controller\Home',
                            'action'     => 'index',
                        ),
                    ),
                ),
                'user_api_get_week' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/api/user/week/:from/:to[/]',
                        'defaults' => array(
                            'controller' => 'Front\Controller\OrderApi',
                            'action'     => 'between',
                        ),
                    ),
                ),
                'user_api_archive' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/api/user/archive/:from/:to[/]',
                        'defaults' => array(
                            'controller' => 'Front\Controller\OrderApi',
                            'action'     => 'archive',
                        ),
                    ),
                ),
                'user_api_add_order' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/api/user/add/:to[/]',
                        'defaults' => array(
                            'controller' => 'Front\Controller\OrderApi',
                            'action'     => 'addOrder',
                        ),
                    ),
                ),
                'user_api_delete_order' => array(
                    'type'    => 'segment',
                    'options' => array(
                        'route'    => '/api/user/delete/:from[/]',
                        'defaults' => array(
                            'controller' => 'Front\Controller\OrderApi',
                            'action'     => 'deleteOrder',
                        ),
                    ),
                ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'front' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'frontLayout' => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),

    'doctrine' => array(
        'driver' => array(
            'front_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Front/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Back\Entity' => 'front_entity',
                )
            )
        )
    ),
);