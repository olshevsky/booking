<?php
namespace Front\Controller;

use Back\Repository\Orders;
use Back\Repository\Users;
use Doctrine\ORM\EntityManager;
use Front\Controller\AuthController;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use DateTime;
use Zend\View\Model\JsonModel;
use Back\Entity\Order;
use Back\Entity\User;
use Zend\View\Model\ViewModel;

class OrderApiController extends AuthController
{
    public function betweenAction()
    {
        $from = $this->params()->fromRoute('from', 0);
        $to = $this->params()->fromRoute('to', 0);

        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var Orders $userRepo */
        $userRepo = $em->getRepository(Order::class);
        $data = $userRepo->getUsersOrdersBetween($this->identity()->getId(),$from, $to);
        $new_from = new DateTime($from);
        $new_to = date_create($to);
        $buffer = $new_from;
        $days = [];

        $orderedDays = [];
        foreach($data as $row)
        {
            $orderedDays[date_format($row['date'], 'Y-m-d')] = 1;
        }

        $days = [];
        $endPoint = abs($new_from->diff($new_to)->format('%R%a'));
        for($i = 0; $i <= $endPoint; $i++)
        {
            $dateOrders = date_format($buffer, 'Y-m-d');
            if(isset($orderedDays[$dateOrders]))
                if(date_diff($buffer, new DateTime())->format('%R%a') <= 0)
                {
                    $days[] = array('date'=> $dateOrders,
                        'status'=> 1
                    );
                }
                else
                {
                    $days[] = array('date'=> $dateOrders,
                        'status'=> 2
                    );
                }
            else
                if(date_diff($buffer, new DateTime())->format('%R%a') <= 0)
                {
                    $days[] = array('date'=> $dateOrders,
                        'status'=> 0
                    );
                }
                else
                {
                    $days[] = array('date'=> $dateOrders,
                        'status'=> 3
                    );
                }
            $buffer->modify('+1 day');
        }
//        return new JsonModel(array('data' => $days));
        return new JsonModel($days);
    }
    public function addOrderAction()
    {
        $to = $this->params()->fromRoute('to', 0);
        $now = time();
        $today = new DateTime();

        if($to == $today->format('Y-m-d') && (intval(date('H',$now))>=10) && (intval(date('i',$now))>=30)){
            return new JsonModel(['message' => 'Nope!']);
        }
        else{
            /* @var $em EntityManager*/
            $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
            $order = new Order();
            $userRepo = $em->getRepository(User::class);
            $to = $this->params()->fromRoute('to', 0);
            $order->setDate(new DateTime($to));
            $order->setUser($userRepo->find($this->identity()->getId()));
            $em->persist($order);
            $em->flush();
            return new JsonModel();
        }
    }
    public function deleteOrderAction()
    {
        $from = $this->params()->fromRoute('from', 0);
        $fromDate = new DateTime($from);
        $now = time();
        $today = new DateTime();

        if((($today>$fromDate)||($from == $today && (intval(date('H',$now))>=10) && (intval(date('i',$now))>=30))))
        {
            return new JsonModel([
                'message' => 'Nope!'
            ]);
        }
        else{
            /* @var $em EntityManager*/
            $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
            /**@var $userRepo Orders */
            $userRepo = $em->getRepository(Order::class);
            $data = $userRepo->deleteUserOrder($this->identity()->getId(), $from);
            return new JsonModel();
        }

    }
    public function archiveAction()
    {
        $from = $this->params()->fromRoute('from', 0);
        $to = $this->params()->fromRoute('to', 0);
        /* @var $em EntityManager*/
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /**@var $usersRepo Orders*/
        $usersRepo = $em->getRepository(Order::class);
        $data = $usersRepo->getUserArchive($this->identity()->getId(), $from, $to);
        $res = [];
        foreach($data as $row){
            $res[] = date_format($row['date'], 'Y-m-d');
        }
        return new JsonModel($res);
    }
}