<?php
namespace Front\Controller;

use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class HomeController extends AuthController
{
    public function indexAction()
    {
        return new ViewModel();
    }
}