<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 7/30/14
 * Time: 1:36 PM
 */

namespace Front\Controller;
use User\Controller\BaseController;
use Zend\Mvc\MvcEvent;

class AuthController extends BaseController{
    public function onDispatch(MvcEvent $e)
    {
        if(is_null($this->identity())){
            return $this->redirect()->toRoute('user_login');
        }
        if(!is_null($this->identity()) && 'user' != $this->identity()->getRole())
        {
            return $this->notFoundAction();
        }
        elseif(!is_null($this->identity()) && $this->identity()->getRole() == 'user' && $this->identity()->getStatus() != 'active')
        {
            return $this->redirect()->toRoute('not_active');
        }
        return parent::onDispatch($e);
    }
} 