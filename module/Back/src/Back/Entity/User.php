<?php
namespace Back\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Album
 * @package Album\Entity
 * @ORM\Entity(repositoryClass="Back\Repository\Users")
 * @ORM\Table(name="users")
 */
class User
{
    const STATUS_WAITING = 'waiting';
    const STATUS_ACTIVE = 'active';
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $login;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $prt;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $status;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $passwd;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $role;

    /**
     * @ORM\OneToMany(targetEntity="Back\Entity\Order", mappedBy="Orders")
     * @var \Back\Entity\Order
     */
    protected $orders;

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $passwd
     * @return $this
     */
    public function setPasswd($passwd)
    {
        $this->passwd = $passwd;
        return $this;
    }

    /**
     * @return string
     */
    public function getPasswd()
    {
        return $this->passwd;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param \Back\Entity\Order $orders
     * @return $this
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return \Back\Entity\Order
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $prt
     * @return $this
     */
    public function setPrt($prt)
    {
        $this->prt = $prt;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrt()
    {
        return $this->prt;
    }
}