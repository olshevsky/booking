<?php
namespace Back\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Album
 * @package Album\Entity
 * @ORM\Entity(repositoryClass="Back\Repository\Orders")
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @param \Back\Entity\date $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \Back\Entity\date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Back\Entity\User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \Back\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var date
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="Back\Entity\User")
     * @var \Back\Entity\User
     */
    protected $user;
}