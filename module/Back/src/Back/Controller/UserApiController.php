<?php
namespace Back\Controller;

use Back\Controller\AuthController;
use Back\Entity\Order;
use Back\Entity\User;
use Back\Repository\Orders;
use Back\Repository\Users;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\JsonModel;
use DateTime;

class UserApiController extends AuthController
{
    public function peopleAction()
    {
        $date = $this->params()->fromRoute('day', 0);
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $result = $em->getRepository('Back\Entity\Order')->findBy(array('date' => new DateTime($date)));

        $data = [];
//        foreach($result as $row)
//        {
//            $data[] = array('id' => $row->getUser()->getId(),
//                'name' => $row->getUser()->getName(),
//            );
//        }
        foreach($result as $row){
            $data[$row->getUser()->getPrt()][] = [
                'id' => $row->getUser()->getId(),
                'name' => $row->getUser()->getName(),
                'part' => $row->getUser()->getPrt()
            ];
        }
        return new JsonModel(array('data' => $data));
    }

    public function newUsersAction(){
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var $newUsersRepo Users*/
        $newUsersRepo = $em->getRepository(User::class);
        $newUsers = $newUsersRepo->getNewUsers();
        return new JsonModel($newUsers);
    }
    public function confirmUserAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus('active');
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
    public function deleteUserAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus('deleted');
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
    public function allUsersAction(){
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $users = $userRepo->findAll();
        $usersList = [];
        foreach($users as $user)
        {
            $usersList[] = array('id'=>$user->getId(),
                'name'=>$user->getName(),
                'login' => $user->getLogin(),
                'status' => $user->getStatus(),
                'role' => $user->getRole(),
                'prt' => $user->getPrt());
        }
        return new JsonModel($usersList);
    }
    public function setStatusAction(){
        $id =$this->params()->fromRoute('id', 0);
        $status =$this->params()->fromRoute('status', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus($status);
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }

    public function personAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $data = [
            'id' => $user->getId(),
            'login' => $user->getLogin(),
            'name' => $user->getName(),
            'prt' => $user->getPrt(),
            'status' => $user->getStatus(),
            'role' => $user->getRole()
        ];
        return new JsonModel($data);
    }
    public function savePersonAction(){
        $request = $this->getRequest();
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($request->getPost('id'));
        $user->setRole(htmlspecialchars($request->getPost('role')));
        $user->setStatus(htmlspecialchars($request->getPost('status')));
        $user->setPrt(htmlspecialchars($request->getPost('prt')));
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
}