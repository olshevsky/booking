<?php
/**
 * Created by PhpStorm.
 * User: owner
 * Date: 7/30/14
 * Time: 2:38 PM
 */

namespace Back\Controller;


use Back\Entity\Order;
use Back\Entity\User;
use Back\Repository\Orders;
use Back\Repository\Users;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\JsonModel;
use DateTime;

class ApiController extends AuthController{
    public function betweenAction()
    {
        $from = $this->params()->fromRoute('from', 0);
        $to = $this->params()->fromRoute('to', 0);

        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var $ordersRepo Orders*/
        $ordersRepo = $em->getRepository(Order::class);
        $data = $ordersRepo->between($from, $to);
        $new_from = new DateTime($from);
        $new_to = date_create($to);
        $buffer = $new_from;
        $days = [];

        $orderedDays = [];
        foreach($data as $row)
        {
            $orderedDays[date_format($row['date'], 'Y-m-d')] = $row['cnt'];
        }

        $days = [];
        $endPoint = abs($new_from->diff($new_to)->format('%R%a'));
        for($i = 0; $i <= $endPoint; $i++)
        {
            $dateOrders = date_format($buffer, 'Y-m-d');
            if(isset($orderedDays[$dateOrders]))
                $days[] = array('date'=> $dateOrders,
                    'count'=>$orderedDays[$dateOrders]
                );
            else
                $days[] = array('date'=> $dateOrders,
                    'count'=> 0
                );
            $buffer->modify('+1 day');
        }
//        return new JsonModel(array('data' => $days));
        return new JsonModel($days);
    }
    public function archiveAction()
    {

        $from = $this->params()->fromRoute('from', 0);
        $to = $this->params()->fromRoute('to', 0);
        /** @var $em EntityManager*/
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var $ordersRepo Orders*/
        $ordersRepo = $em->getRepository(Order::class);
        $data = $ordersRepo->getArchive($from, $to);

        $days = [];

        foreach($data as $row)
        {
            $days[] = array('date' => date_format($row['date'], 'Y-m-d'),
                            'count' => $row['cnt']);
        }

        return new JsonModel($days);
    }
    public function weekAction()
    {
        $ft = new DateTime();
        $ps = new DateTime();
        $ft->modify('+7 day');
        $ps->modify('-7 day');
        $ft = date_format($ft, 'Y-m-d');
        $ps = date_format($ps, 'Y-m-d');

        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var $ordersRepo Orders */
        $ordersRepo = $em->getRepository(Order::class);
        $data = $ordersRepo->between($ft,$ps);
        $days = [];
        foreach($data as $row)
        {
//            $days[date_format($row['date'], 'Y-m-d')] = $row['cnt'];
            $days[] = array('date'=>date_format($row['date'], 'Y-m-d'),
                            'count' => $row['cnt']);
        }
        return new JsonModel(array('data' => $days));
    }
    public function peopleAction()
    {
        $date = $this->params()->fromRoute('day', 0);
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $result = $em->getRepository('Back\Entity\Order')->findBy(array('date' => new DateTime($date)));

        $data = [];
        foreach($result as $row)
        {
            $data[] = array('id' => $row->getUser()->getId(),
                            'name' => $row->getUser()->getName(),
                            );
        }
        return new JsonModel(array('data' => $data));
    }
    public function deletePersonAction()
    {
        // /api/addPerson/:personId/to/:date
        $personId = $this->params()->fromRoute('personId', 0);
        $date = $this->params()->fromRoute('date', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $order = $em->getRepository(Order::class)->findOneBy(
            [
                'user' => $personId,
                'date' => new DateTime($date),
            ]
        );
        if($order) {
            $em->remove($order);

            $em->flush();

            return new JsonModel(
                [
                    'success' => true
                ]
            );
        } else {
            return new JsonModel(
                [
                    'success' => false
                ]
            );
        }
    }
    public function addPersonAction()
    {
        $personId = $this->params()->fromRoute('personId', 0);
        $date = $this->params()->fromRoute('date', 0);

        $message = "Adding...";
        /** @var $em EntityManager*/
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $newOrder = new Order();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneBy([
            'id' => $personId
        ]);
        $newOrder->setUser($user);
        $newOrder->setDate(new DateTime($date));
        $em->persist($newOrder);
        if($em->flush())
            $message = "Added!";

        return new JsonModel(
            [
                'message' => $message
            ]
        );
    }
    public function personsAction()
    {
        $date =$this->params()->fromRoute('date', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /**@var $ordersRepo Orders*/
        $ordersRepo = $em->getRepository(Order::class);
        $persons = $ordersRepo->getNotOrderedPersonsOnDate($date);
        $returned = [];
        foreach($persons as $person)
        {
            $returned[] = array('id' => $person['id'],
                                'name' => $person['name']
                                );
        }
        return new JsonModel($returned);
    }
    public function newUsersAction(){
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        /** @var $newUsersRepo Users*/
        $newUsersRepo = $em->getRepository(User::class);
        $newUsers = $newUsersRepo->getNewUsers();
        return new JsonModel($newUsers);
    }
    public function confirmUserAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus('active');
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
    public function deleteUserAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus('deleted');
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
    public function allUsersAction(){
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $users = $userRepo->findAll();
        $usersList = [];
        foreach($users as $user)
        {
            $usersList[] = array('id'=>$user->getId(),
                                 'name'=>$user->getName(),
                                 'login' => $user->getLogin(),
                                 'status' => $user->getStatus(),
                                 'role' => $user->getRole(),
                                 'prt' => $user->getPrt());
        }
        return new JsonModel($usersList);
    }
    public function setStatusAction(){
        $id =$this->params()->fromRoute('id', 0);
        $status =$this->params()->fromRoute('status', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $user->setStatus($status);
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }

    public function personAction(){
        $id =$this->params()->fromRoute('id', 0);
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($id);
        $data = [
            'id' => $user->getId(),
            'login' => $user->getLogin(),
            'name' => $user->getName(),
            'prt' => $user->getPrt(),
            'status' => $user->getStatus(),
            'role' => $user->getRole()
        ];
        return new JsonModel($data);
    }
    public function savePersonAction(){
        $request = $this->getRequest();
        /** @var $em EntityManager */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->find($request->getPost('id'));
        $user->setRole($request->getPost('role'));
        $user->setStatus($request->getPost('status'));
        $user->setPrt($request->getPost('prt'));
        $em->persist($user);
        $em->flush();
        return new JsonModel();
    }
} 