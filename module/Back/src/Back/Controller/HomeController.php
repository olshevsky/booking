<?php
namespace Back\Controller;
use DateTime;
use Doctrine\ORM\EntityManager;
use User\Controller\BaseController;
use Zend\Db\Sql\Ddl\Column\Date;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Back\Entity;

class HomeController extends AuthController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    public function showAction(){
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');

        $date = $this->params()->fromRoute('date', 0);

        if(!isset($date) || $date == 'today')
        {
            $today = new DateTime();
            $date = $today;
            $date = date_format($date, 'Y-m-d');
            $data = $em->getRepository('Back\Entity\Order')->findBy(array('date' => $today));
        }
        elseif($date == 'special')
        {
            $date = "";
            $date .= trim($this->params()->fromRoute('year', 0)).'-';
            $date .= trim($this->params()->fromRoute('month', 0)).'-';
            $date .= trim($this->params()->fromRoute('day', 0));
            try {
                $day = new DateTime($date);
            } catch(\Exception $e) {
                return $this->notFoundAction();
            }
            $data = $em->getRepository('Back\Entity\Order')->findBy(array('date' => $day));
        }
        return new ViewModel(array('orders' => $data, 'date' => $date));
    }
    public function weekAction()
    {
        $weekDays = array('', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');
        $ft = new DateTime();
        $ps = new DateTime();

        $ft->modify('+7 day');
        $ps->modify('-7 day');

        $ft = date_format($ft, 'Y-m-d');
        $ps = date_format($ps, 'Y-m-d');

        /** @var EntityManager $em */
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $query = $em->createQuery("SELECT DISTINCT o.date, count(o.id) as cnt FROM Back\Entity\Order o WHERE o.date BETWEEN '".$ps."' AND '".$ft."' GROUP BY o.date");
        $data = $query->getResult();

        $days = [];
        foreach($data as $row)
        {
            $days[date_format($row['date'], 'Y-m-d')] = $row['cnt'];
        }
        return new ViewModel(array('data' => $days, 'days_of_week' => $weekDays));
    }
    public function archiveAction()
    {
        $em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        $query = $em->createQuery("SELECT DISTINCT o.date, count(o.id) as cnt FROM Back\Entity\Order o GROUP BY o.date");
        $data = $query->getResult();

        $days = [];

        foreach($data as $row)
        {
            $days[date_format($row['date'], 'Y-m-d')] = $row['cnt'];
        }

        return new ViewModel(array('data' => $days));
    }
}