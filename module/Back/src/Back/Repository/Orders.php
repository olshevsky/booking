<?php
namespace Back\Repository;

use Back\Entity\User;
use Back\Entity\Order;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class Orders extends EntityRepository{
    public function between($from, $to){
        $query = $this->_em->createQuery("SELECT DISTINCT o.date, count(o.id) as cnt FROM Back\Entity\Order o WHERE o.date BETWEEN '".$from."' AND '".$to."' GROUP BY o.date");
        $data = $query->getResult();
        return $data;
    }
    public function getNotOrderedPersonsOnDate($date){
        $query = $this->_em->createQuery("SELECT u.id, u.name FROM Back\Entity\User u WHERE u.id NOT IN(SELECT u2.id FROM Back\Entity\User u2, Back\Entity\Order o2 WHERE o2.user = u2 AND o2.date = '".$date."') AND u.status='active' ");
        return $query->getResult();
    }
    public function getArchive($from, $to){
        $query = $this->_em->createQuery("SELECT DISTINCT o.date, count(o.id) as cnt FROM Back\Entity\Order o WHERE o.date BETWEEN '".$from."' AND '".$to."' GROUP BY o.date");
        return $query->getResult();
    }




    public function getUserArchive($id, $from, $to)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.user = :usr')
            ->andWhere('o.date BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('usr', $id)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function deleteUserOrder($id, $date)
    {
        $query = $this->_em->createQuery("DELETE FROM Back\Entity\Order o WHERE o.user = '".$id."'  AND o.date = '" . $date . "'");
        return $query->getResult();
    }

    public function getUsersOrdersBetween($id, $from, $to)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.user = :usr')
            ->andWhere('o.date BETWEEN :from AND :to')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('usr', $id)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

}