<?php
namespace Back\Repository;

use Back\Entity\User;
use Back\Entity\Order;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class Users extends EntityRepository
{
    /**
     * @return User[]
     */
    public function getNewUsers()
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.status = :uStatus')
            ->setParameter('uStatus', User::STATUS_WAITING)
            ->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
} 