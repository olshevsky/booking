<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Back\Controller\Home' => 'Back\Controller\HomeController',
            'Back\Controller\Api' => 'Back\Controller\ApiController',
            'Back\Controller\UserApi' => 'Back\Controller\UserApiController',
            'Back\Controller\OrderApi' => 'Back\Controller\OrdersApiController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'manager' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/manager[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\Home',
                        'action'     => 'index',
                    ),
                ),
            ),
            'show_orders_by_day' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/orders/show[/][/:date][/:year][/:month][/:day][/]',
                    'constraints' => array(
                        'date' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'year' => '[0-9_-]+',
                        'month' => '[0-9_-]+',
                        'day' => '[0-9_-]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Back\Controller\Home',
                        'action'     => 'show',
                    ),
                ),
            ),
            'all_orders' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/week[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\Home',
                        'action' => 'week'
                    ),
                ),
            ),
            'archive_orders' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/archive[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\Home',
                        'action' => 'archive'
                    ),
                ),
            ),
            'delete_person' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/deletePerson/:personId/from/:date',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'deletePerson'
                    ),
                ),
            ),
            'add_person' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/addPerson/:personId/to/:date',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'addPerson'
                    ),
                ),
            ),
            'api_archive_orders' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/archive/:from/:to',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'archive'
                    ),
                ),
            ),
            'api_week_orders' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/week[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'week'
                    ),
                ),
            ),
            'api_between' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/show/from/:from/to/:to[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'between'
                    ),
                ),
            ),
            'api_show_people_by_date' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/people/:day[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'people'
                    ),
                ),
            ),
            'api_all_persons' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/persons/:date[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\OrderApi',
                        'action' => 'persons'
                    ),
                ),
            ),
            'api_new_users' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/new[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'newUsers'
                    ),
                ),
            ),
            'api_confirm_user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/confirm/:id[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'confirmUser'
                    ),
                ),
            ),
            'api_delete_user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/delete/:id[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'deleteUser'
                    ),
                ),
            ),
            'api_all_users' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/all[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'allUsers'
                    ),
                ),
            ),
            'api_set_status' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/set/:id/:status[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'setStatus'
                    ),
                ),
            ),
            'api_get_person' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/person/:id[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'person'
                    ),
                ),
            ),
            'api_save_person' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/users/save[/]',
                    'defaults' => array(
                        'controller' => 'Back\Controller\UserApi',
                        'action' => 'savePerson'
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'back' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'admin/layout' => __DIR__ . '/../view/layout/layout.phtml',
        ),
    ),

    'doctrine' => array(
        'driver' => array(
            'front_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Back/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Back\Entity' => 'front_entity',
                )
            )
        )
    ),
);