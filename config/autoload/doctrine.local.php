<?php
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'     => 'localhost',
                    'port'     => '3306',
                    'user'     => 'root',
                    'password' => '1',
                    'dbname'   => 'food',
                )
            )
        ),
        'authentication' => [
            'orm_default' => [
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'Back\Entity\User',
                'identity_property' => 'login',
                'credential_property' => 'passwd',
                'credential_callable' => function ($user, $passwordGiven) {
                        return $user->getPasswd() === md5(trim($passwordGiven));
//                        return $user->getPasswd() === md5(trim($passwordGiven));
                    }
            ],
        ]
    ),
);