<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    'service_manager' => array(
        'factories' => array(
            'mail.transport' => function ($serviceManager) {
                    $config = $serviceManager->get('Config');
                    $transport = new $config['mail']['transport']['type'];
                    $options = new $config['mail']['transport']['options_class']($config['mail']['transport']['options']);
                    $transport->setOptions($options);
                    unset($config, $options);

                    return $transport;
                },
//            'Zend\Authentication\AuthenticationService' => function ($serviceManager) {
//                    // If you are using DoctrineORMModule:
//                    return $serviceManager->get('Auth');
//                },
        ),
        'aliases' => [
            'em' => 'doctrine.entitymanager.orm_default',
        ],
    ),
);
