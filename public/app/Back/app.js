var app = angular.module('Manager',['ngRoute','angularMoment']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/archive', {
            templateUrl: '/app/Back/partials/archive.html',
            controller: 'archiveCtrl'
        });

    $routeProvider.
        when('/new-users', {
            templateUrl: '/app/Back/partials/new-users.html',
            controller: 'newUsersCtrl'
        });

    $routeProvider.
        when('/users/edit/:userId', {
            templateUrl: '/app/Back/partials/edit-user.html',
            controller: 'editUserCtrl'
        });

    $routeProvider.
        when('/all-users', {
            templateUrl: '/app/Back/partials/all-users.html',
            controller: 'allUsersCtrl'
        });

        $routeProvider.
            when('/', {
                templateUrl: '/app/Back/partials/index.html',
                controller: 'indexCtrl'
            }).otherwise({
                redirectTo: '/'
            });
    }]);
