app.controller('indexCtrl', function ($scope, $http) {
//    $scope.date = moment();
//    $scope.firstDay = moment($scope.date).weekday(1).format('YYYY-MM-DD');
//    $scope.lastDay = moment($scope.date).weekday(7).format('YYYY-MM-DD');

    $scope.date = moment();
    $scope.firstDay = moment().subtract('d', 7).format('YYYY-MM-DD');
    $scope.lastDay = moment($scope.date).add('d', 7).format('YYYY-MM-DD');

    $scope.persons = [];
    getData = function () {
        $http.get('/api/show/from/' + $scope.firstDay + '/to/' + $scope.lastDay + '/').success(function (data) {
            $scope.days = data;
        });
    }
    getData();
    $scope.days = [];
    $scope.lastDate;
    $scope.viewPeople = function ($date) {
        $scope.confirmed = null;
        $scope.per = null;
        $scope.lastDate = $date;
        $http.get('/api/people/' + $date).success(function (data) {
            $scope.people = data;
            return true;
        });
        return false;
    }
    $scope.deleteOrder = function ($personId, $date) {
        // Delete person action in API -  /api/deletePerson/:personId/from/:date
        $http.get('/api/deletePerson/' + $personId + '/from/' + $date).success(function () {
            getData();
            $scope.viewPeople($scope.lastDate);
        });
    }
    $scope.prev = function () {
//        $scope.date.subtract('d',7);
//        $scope.firstDay = moment($scope.date).weekday(1).format('YYYY-MM-DD');
//        $scope.lastDay = moment($scope.date).weekday(7).format('YYYY-MM-DD');
        $scope.lastDay = moment($scope.lastDay).subtract('d', 15).format('YYYY-MM-DD');
        $scope.firstDay = moment($scope.firstDay).subtract('d', 15).format('YYYY-MM-DD');
        getData();
    }
    $scope.next = function () {
//        $scope.date.add('d',7);
//        $scope.firstDay = moment($scope.date).weekday(1).format('YYYY-MM-DD');
//        $scope.lastDay = moment($scope.date).weekday(7).format('YYYY-MM-DD');
        $scope.lastDay = moment($scope.lastDay).add('d', 15).format('YYYY-MM-DD');
        $scope.firstDay = moment($scope.firstDay).add('d', 15).format('YYYY-MM-DD');
        getData();
    }
    $scope.addPerson = function () {
        $scope.persons = [];
        $scope.confirmed = true;
        $http.get('/api/persons/' + $scope.lastDate).success(function (data) {
            $scope.persons = data;
            return true;
        });
    }
    $scope.addingPerson = function ($person, $date) {
        // Add person action in API - /api/addPerson/:personId/to/:date
        $http({method: 'POST', url: '/api/addPerson/' + $person.id + '/to/' + $date, data: [$date]})
            .success(function (url, data) {
                getData();
                $scope.viewPeople($scope.lastDate);
                $scope.per = null;
                $scope.confirmed = null;
            });
    }
});

app.controller('archiveCtrl', function ($scope, $http) {
    $scope.dateFrom = moment($scope.date).weekday(1).format('YYYY-MM-DD');
    $scope.dateTo = moment($scope.date).weekday(7).format('YYYY-MM-DD');
    $scope.persons = [];

    $scope.showByDate = function (dateFrom, dateTo) {
        mF = moment(dateFrom);
        mT = moment(dateTo);

        if (mT.diff(mF, 'days') < 0) {
            $scope.dateFrom = moment($scope.date).weekday(1).format('YYYY-MM-DD');
            $scope.dateTo = moment($scope.date).weekday(7).format('YYYY-MM-DD');
            alert("Ты что творишь, ирод? Дату роверь, ибо негоже что бы начальная дата превосходила конечную! Вот!");
        }
        $http.get('/api/archive/' + dateFrom + '/' + dateTo).success(function (data) {
            $scope.persons = data;
            return true;
        });
    }
    $scope.showByDate($scope.dateFrom, $scope.dateTo);
});

app.controller('newUsersCtrl', function ($scope, $http) {
    $scope.newUsers = [];
    getNewUsers = function () {
        $http.get('/api/users/new').success(function (data) {
            $scope.newUsers = data;
            return true;
        });
    }
    getNewUsers();
    $scope.deleteUser = function (id) {
        $http.get('/api/users/delete/' + id).success(function (data) {
            return true;
        });
        getNewUsers();
    }
    $scope.confirmUser = function (id) {
        $http.get('/api/users/confirm/' + id).success(function (data) {
            return true;
        });
        getNewUsers();
    }
});

app.controller('allUsersCtrl', function ($scope, $http) {
    $scope.allUsers = [];
    getAllUsers = function () {
        $http.get('/api/users/all').success(function (data) {
            $scope.allUsers = data;
            return true;
        });
    }
    getAllUsers();
    $scope.deleteUser = function (id) {
        $http.get('/api/users/delete/' + id).success(function (data) {
            return true;
        });
        getAllUsers();
    }
});

app.controller('editUserCtrl', function ($scope, $http, $routeParams) {
    $scope.params = $routeParams;
    $scope.person = [];
    $scope.obj = [];

    $scope.statuses = ['active', 'waiting', 'deleted'];
    $scope.roles = ['manager', 'user'];

    getPerson = function () {
        $http.get('/api/users/person/' + $scope.params.userId).success(function (data) {
            $scope.person = data;
            return true;
        });
    }
    getPerson();

    $scope.save = function (person) {
        $http({
            method: 'POST', url: '/api/users/save', data: $.param(person),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (url, data) {
            $scope.saved = true;
            });
    }
});