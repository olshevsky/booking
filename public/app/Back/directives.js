app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
//                    scope.date = date;
//                    scope.$apply();
                    var ngModelName = this.attributes['ng-model'].value;
                    scope[ngModelName] = date;
                    scope.$apply();
                }
            });
        }
    };
});