var app = angular.module('User',['ngRoute','angularMoment']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/archive', {
            templateUrl: '/app/Front/partials/archive.html',
            controller: 'archiveCtrl'
        });

        $routeProvider.
            when('/', {
                templateUrl: '/app/Front/partials/index.html',
                controller: 'indexCtrl'
            }).otherwise({
                redirectTo: '/'
            });
    }]);
