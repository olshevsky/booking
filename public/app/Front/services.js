'use strict';
app.factory('UsersOrders',['$http', '$rootScope', function($http, $rootScope){
    var orders = [];
    var archive = [];
    var service = {};

    var from = moment().subtract('d',7).format('YYYY-MM-DD');
    var to = moment().add('d',7).format('YYYY-MM-DD');

    function getOrders(from, to) {
        $http({method: 'GET', url: '/api/user/week/'+from+'/'+to+'/'})
            .success(function(data, status, headers, config) {
                orders = data;
                $rootScope.$broadcast('orders:updated');
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    getOrders(from, to);

    service.getWeeks = function(){
        return orders;
    }

    service.addOrder = function(date){
        $http({method: 'GET', url: '/api/user/add/'+date})
            .success(function(data, status, headers, config) {
                angular.forEach(orders, function(value) {
                    if (value.date === date) {
                        value.status = 1;
                        return false;
                    }
                });
                $rootScope.$broadcast('orders:updated');
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }

    service.deleteOrder = function(date){
        $http({method: 'GET', url: '/api/user/delete/'+date})
            .success(function(data, status, headers, config) {
                angular.forEach(orders, function(value ,i) {
                    if (value.date === date) {
                        value.status = 0;
                        return false;
                    }
                });
                $rootScope.$broadcast('orders:updated');
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }
    return service;
}]);

app.factory('UserArchive',['$http', '$rootScope', function($http, $rootScope){
    var archive = [];
    var service = {};

    var from = moment().subtract('d',7).format('YYYY-MM-DD');
    var to = moment().add('d',7).format('YYYY-MM-DD');

    function archived(from,to){
        $http({method: 'GET', url: '/api/user/archive/'+from+'/'+to+'/'})
            .success(function(data, status, headers, config) {
                archive = data;
                $rootScope.$broadcast('archive:updated');
            })
            .error(function(data, status, headers, config) {
                console.log(data);
            });
    }

    archived(from, to);

    service.getArchive = function(from,to){
        archived(from,to);
        return archive;
    }

    return service;
}])