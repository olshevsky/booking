app.controller('indexCtrl', function ($scope, $http, UsersOrders, $rootScope) {
    $scope.date = moment();
    $scope.firstDay = moment().subtract('d',7).format('YYYY-MM-DD');
    $scope.lastDay = moment($scope.date).add('d',7).format('YYYY-MM-DD');

    $scope.days = [];
    $scope.days = UsersOrders.getWeeks();

    $rootScope.$on('orders:updated', function() {
        $scope.days = UsersOrders.getWeeks();
    });

    $scope.addOrder = function(date){
        if((date == moment().format('YYYY-MM-DD') && moment().hour()>10) || (date == moment().format('YYYY-MM-DD') && moment().hour() == 10 && moment().minute()>30))
        {
            alert("Кто не успел, тот остаётся голодным!");
            console.log("Кто не успел, тот остаётся голодным!");
        }
        else
            UsersOrders.addOrder(date);
    }

    $scope.removeOrder = function(date){
        if(date == moment().format('YYYY-MM-DD') && moment().hour() > 11)
            alert("Назад пути нет!!");
        else
            UsersOrders.deleteOrder(date);
    }

    $scope.pastDateAlert = function(){
        alert('Что было - то прошло...');
    }
});

app.controller('archiveCtrl', function ($scope, $http) {
    $scope.dateFrom = moment($scope.date).weekday(1).format('YYYY-MM-DD');
    $scope.dateTo = moment($scope.date).weekday(7).format('YYYY-MM-DD');
    $scope.archive = [];

    $scope.showByDate = function(dateFrom, dateTo)
    {
        mF = moment(dateFrom);
        mT = moment(dateTo);

        if(mT.diff(mF, 'days') < 0)
        {
            $scope.dateFrom = moment($scope.date).weekday(1).format('YYYY-MM-DD');
            $scope.dateTo = moment($scope.date).weekday(7).format('YYYY-MM-DD');
            alert("Ты что творишь, ирод? Дату роверь, ибо негоже что бы начальная дата превосходила конечную! Вот!");
        }
        $http.get('/api/user/archive/'+$scope.dateFrom+'/'+$scope.dateTo+'/').success(function(data) {
            $scope.archive = data;
            return true;
        });
    }
    $scope.showByDate($scope.dateFrom, $scope.dateTo);
});